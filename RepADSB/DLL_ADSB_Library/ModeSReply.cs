﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{


    public class ModeSReply
    {
        private byte downlink_format; // 0-31
        private byte first_field; // the 3 bits after downlink format
        private sbyte[] icao24; // 3 bytes
        private sbyte[] payload; // 3 or 10 bytes
        private sbyte[] parity; // 3 bytes
        private bool noCRC;


        public enum subtype
        {
            MODES_REPLY, // unknown mode s reply
            SHORT_ACAS,
            ALTITUDE_REPLY,
            IDENTIFY_REPLY,
            ALL_CALL_REPLY,
            LONG_ACAS,
            EXTENDED_SQUITTER,
            MILITARY_EXTENDED_SQUITTER,
            COMM_B_ALTITUDE_REPLY,
            COMM_B_IDENTIFY_REPLY,
            COMM_D_ELM,
            // ADS-B subtypes
            ADSB_AIRBORN_POSITION,
            ADSB_SURFACE_POSITION,
            ADSB_AIRSPEED,
            ADSB_EMERGENCY,
            ADSB_TCAS,
            ADSB_VELOCITY,
            ADSB_IDENTIFICATION,
            ADSB_STATUS
        }
        private subtype type;

        /**
	 * polynomial for the cyclic redundancy check<br>
	 * Note: we assume that the degree of the polynomial
	 * is divisible by 8 (holds for Mode S) and the msb is left out
	 */
        public static short[] CRC_polynomial = {
        (short)0xFF,
        (short)0xF4,
        (short)0x09 // according to Annex 10 V4
	};

        /**
         * @param msg raw message as byte array
         * @return calculated parity field as 3-byte array. We used the implementation from<br>
         *         http://www.eurocontrol.int/eec/gallery/content/public/document/eec/report/1994/022_CRC_calculations_for_Mode_S.pdf
         */
        //TODO: REWRITE IT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!( trobles with sbyte\byte TEST IT + do smt with CRC_polynomial)
        public static sbyte[] calcParity(sbyte[] msg)
        {
            sbyte[] pi = new sbyte[CRC_polynomial.Length];
            Array.Copy(msg, pi, CRC_polynomial.Length);
            bool invert;
            int byteidx, bitshift;
            for (int i = 0; i < msg.Length * 8; ++i)
            { // bit by bit
                invert = ((pi[0] & 0x80) != 0);

                // shift left
                pi[0] <<= 1;
                for (int b = 1; b < CRC_polynomial.Length; ++b)
                {
                    pi[b - 1] |= (sbyte)((pi[b] >> 7) & 0x1);
                    pi[b] <<= 1;
                }

                // get next bit from message
                byteidx = ((CRC_polynomial.Length * 8) + i) / 8;
                bitshift = 7 - (i % 8);
                if (byteidx < msg.Length)
                    pi[pi.Length - 1] |= (sbyte)((msg[byteidx] >> bitshift) & 0x1);
                // xor
                if (invert)
                {
                    var curArray = Array.ConvertAll(pi, item => (short)item);

                    for (int b = 0; b < CRC_polynomial.Length; ++b)
                        curArray[b] ^= CRC_polynomial[b];

                    pi = Array.ConvertAll(curArray, item => (sbyte)item);
                }
            }
            sbyte[] parcel = new sbyte[CRC_polynomial.Length];
            Array.Copy(pi, parcel, CRC_polynomial.Length);
            return parcel;
        }



        protected ModeSReply() { }

        public ModeSReply(String raw_message)
        {
            parseMessage(raw_message, false);
        }

        /**
         * We assume the following message format:<br>
         * | DF (5) | FF (3) | Payload (24/80) | PI/AP (24) |
         * 
         * @param raw_message Mode S message in hex representation
         * @param noCRC indicates whether the CRC has been subtracted from the parity field
         * @throws BadFormatException if message has invalid length or payload does
         * not match specification or parity has invalid length
         */
        public ModeSReply(String raw_message, bool noCRC)
        {
            parseMessage(raw_message, noCRC);
        }


        public static int getExpectedLength(byte downlink_format)
        {
            if (downlink_format < 16) return 7;
            else return 14;
        }

        protected void setType(subtype subtype)
        {
            this.type = subtype;
        }

        public byte getDownlinkFormat()
        {
            return downlink_format;
        }

        public void parseMessage(string raw_message, bool noCRC)
        {
            // check format invariants
            int length = raw_message.Length;
            this.noCRC = noCRC;

            if (length != 14 && length != 28) // initial test
                //TODO: exaption throw new BadFormatException("Raw message has an invalid length of "+length, raw_message);
                Console.WriteLine("Raw message has an invalid length of " + length, raw_message);

            //downlink_format = (byte) (Encoding.ASCII.GetBytes(raw_message.Substring(0, 2), 16));
            downlink_format = Convert.ToByte(raw_message.Substring(0, 2), 16);
            first_field = (byte)(downlink_format & 0x7);
            downlink_format = (byte)(downlink_format >> 3 & 0x1F);

            if (length != getExpectedLength(downlink_format) << 1)
            {
                //TODO: exaption
            }

            // extract payload
            payload = new sbyte[(length - 8) / 2];
            for (int i = 2; i < length - 6; i += 2)

                payload[(i - 2) / 2] = Convert.ToSByte(raw_message.Substring(i, 2), 16);

            // extract parity field
            parity = new sbyte[3];
            for (int i = length - 6; i < length; i += 2)
                parity[(i - length + 6) / 2] = Convert.ToSByte(raw_message.Substring(i, 2), 16);

            // extract ICAO24 address
            icao24 = new sbyte[3];
            switch (downlink_format)
            {
                case 0: // Short air-air (ACAS)
                case 4: // Short altitude reply
                case 5: // Short identity reply
                case 16: // Long air-air (ACAS)
                case 20: // Long Comm-B, altitude reply
                case 21: // Long Comm-B, identity reply
                case 24: // Long Comm-D (ELM)
                    //TODO: FIX THAT BLOCK icao24 = noCRC ? parity : tools.xor(calcParity(), parity);
                    icao24 = noCRC ? parity : Tools.xor(calcParity(), parity);
                    break;

                case 11: // all call replies
                case 17:
                case 18: // Extended squitter
                    for (int i = 0; i < 3; i++)
                        icao24[i] = payload[i];
                    break;
                    //default: // unkown downlink format
                    // leave everything 0
            }

            setType(subtype.MODES_REPLY);
        }


        public ModeSReply(ModeSReply reply)
        {
            downlink_format = reply.getDownlinkFormat();
            first_field = reply.getFirstField();
            icao24 = reply.getIcao24();
            payload = reply.getPayload();
            parity = reply.getParity();
            type = reply.getType();
        }

        /**
         * @return the subtype
         */
        public subtype getType()
        {
            return type;
        }



        /**
         * Note: Should only be used by subtype classes
         * @return the first field (three bits after downlink format)
         */
        protected byte getFirstField()
        {
            return first_field;
        }

        /**
         * @return the icao24 as an 3-byte array
         */
        public sbyte[] getIcao24()
        {
            return icao24;
        }

        /**
         * @return payload as 3- or 10-byte array containing the Mode S
         * reply without the first and the last three bytes. 
         */
        public sbyte[] getPayload()
        {
            return payload;
        }

        /**
         * @return parity field from message as 3-byte array
         */
        public sbyte[] getParity()
        {
            return parity;
        }



        public sbyte[] calcParity()
        {
            sbyte[] message = new sbyte[payload.Length + 1];

            message[0] = (sbyte)(downlink_format << 3 | first_field);
            for (sbyte b = 0; b < payload.Length; ++b)
                message[b + 1] = payload[b];

            return calcParity(message);
        }

        /**
         * Re-builds the message from the fields and returns it as a hex string
         * @return the reply as a hex string
         */
        public String getHexMessage()
        {
            sbyte[] msg = new sbyte[4 + payload.Length];
            msg[0] = (sbyte)(downlink_format << 3 | first_field);
            for (int i = 0; i < payload.Length; ++i) msg[1 + i] = payload[i];
            sbyte[] crc = noCRC ? Tools.xor(getParity(), calcParity()) : getParity();
            for (int i = 0; i < 3; ++i) msg[1 + payload.Length + i] = crc[i];
            return Tools.toHexString(msg);
        }

        /**
         * Important note: use this method for extended
         * squitter/ADS-B messages (DF 17, 18) only! Other messages may have
         * their parity field XORed with an ICAO24 transponder address
         * or an interrogator ID.
         * @return true if parity in message matched calculated parity
         */
        public bool checkParity()
        {
            return Tools.areEqual(calcParity(), getParity());
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        public String toString()
        {
            return "Mode S Reply:\n" +
                    "\tDownlink format:\t" + getDownlinkFormat() + "\n" +
                    "\tICAO 24-bit address:\t" + Tools.toHexString(getIcao24()) + "\n" +
                    "\tPayload:\t\t" + Tools.toHexString(getPayload()) + "\n" +
                    "\tParity:\t\t\t" + Tools.toHexString(getParity()) + "\n" +
                    "\tCalculated Parity:\t" + Tools.toHexString(calcParity());
        }

        public bool equals(Object o)
        {
            if (o == null) return false;
            if (o == this) return true;
            if (o.GetType() != GetType()) return false;

            ModeSReply other = (ModeSReply)o;

            // same type?
            if (this.getDownlinkFormat() != other.getDownlinkFormat())
                return false;

            // most common
            if (this.getDownlinkFormat() == 11 &&
                    !Tools.areEqual(this.getIcao24(), other.getIcao24()))
                return false;

            // ads-b
            if (this.getDownlinkFormat() == 17 &&
                    !Tools.areEqual(this.getIcao24(), other.getIcao24()))
                return false;
            if (this.getDownlinkFormat() == 18 &&
                    !Tools.areEqual(this.getIcao24(), other.getIcao24()))
                return false;

            // check the full payload
            if (!Tools.areEqual(this.getPayload(), other.getPayload()) ||
                    this.getFirstField() != other.getFirstField())
                return false;

            // and finally the parity
            if (Tools.areEqual(this.getParity(), other.getParity()))
                return true;

            // Note: the following checks are necessary since some receivers set
            // the parity field to the remainder of the CRC (0 if correct)
            // while others do not touch it. This combination should be extremely
            // rare so the performance can be more or less neglected.

            if (Tools.areEqual(this.getParity(), other.calcParity()))
                return true;

            if (Tools.areEqual(this.calcParity(), other.getParity()))
                return true;

            if (this.getDownlinkFormat() == 11)
            {
                // check interrogator code
                if (Tools.areEqual(Tools.xor(calcParity(), getParity()),
                        other.getParity()))
                    return true;

                if (Tools.areEqual(Tools.xor(other.calcParity(),
                        other.getParity()), this.getParity()))
                    return true;
            }

            return Tools.areEqual(this.getIcao24(), other.getParity()) ||
                    Tools.areEqual(this.getParity(), other.getIcao24());
        }


        public int hashCode()
        {
            // same method used by String
            int sum = downlink_format << 3 | first_field;
            for (int i = 0; i < payload.Length; ++i)
                sum += payload[i] * 31 ^ (payload.Length - i);

            sbyte[] effective_partiy = parity;
            if (noCRC) effective_partiy = Tools.xor(parity, calcParity());

            for (int i = 0; i < effective_partiy.Length; ++i)
                sum += effective_partiy[i] * 31 ^ (payload.Length + effective_partiy.Length - i);
            return sum;
        }


    }
}
