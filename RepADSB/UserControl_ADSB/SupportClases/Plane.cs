﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ADSBControl
{
    public class Plane : INotifyPropertyChanged
    {
        private string icao24;
        private string countryFlag;
        private string typeOfPlane;
        private string dataField;
        private string country;

        private double latitude;
        private double longitude;
        //had to
        private string altitude;



        public string ICAO
        {
            get { return icao24; }
            set
            {
                icao24 = value;
                OnPropertyChanged("ICAO");
            }
        }

        public string Country
        {
            get { return country; }
            set
            {
                country = value;
                OnPropertyChanged("Country");
            }
        }

        public string CountryFlag
        {
            get { return countryFlag; }
            set
            {
                countryFlag = value;
                OnPropertyChanged("CountryFlag");
            }
        }

        public string TypeOfPlane
        {
            get { return typeOfPlane; }
            set
            {
                typeOfPlane = value;
                OnPropertyChanged("TypeOfPlane");
            }
        }

        public string DataField
        {
            get { return dataField; }
            set
            {
                dataField = value;
                OnPropertyChanged("DataField");
            }
        }

        public double Latitude
        {
            get { return latitude; }
            set
            {
                latitude = value;
                OnPropertyChanged("Latitude");
            }
        }

        public double Longitude
        {
            get { return longitude; }
            set
            {
                longitude = value;
                OnPropertyChanged("Longitude");
            }
        }

        public string Altitude
        {
            get { return altitude; }
            set
            {
                altitude = value;
                OnPropertyChanged("Altitude");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
