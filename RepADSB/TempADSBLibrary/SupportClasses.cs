﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempADSBLibrary
{
    public struct TDataADSBReceiver
    {
        public int iID;           // номер записи
        public string sICAO;      // номер воздушного объекта
        public string sLatitude;  // широта
        public string sLongitude; // долгота
        public string sAltitude;  // высота
        public string sDatetime;  // дата, время
    }


    public struct Struct_ADSB_Receiver
    {
        public int iID;           // номер записи
        public string sICAO;      // номер воздушного объекта
        public string sLatitude;  // широта
        public string sLongitude; // долгота
        public string sAltitude;  // высота
        public string sDatetime;  // дата, время
    }
}
