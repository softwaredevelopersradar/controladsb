﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public class Position
    {
        private Double longitude;
        private Double latitude;
        private Double altitude;
        private bool reasonable;

        public Position()
        {
            longitude = 0;
            latitude = 0;
            altitude = 0;
            setReasonable(true); // be optimistic :-)
        }

        /**
         * @param lon longitude in decimal degrees
         * @param lat latitude in decimal degrees
         * @param alt altitude in meters
         */
        public Position(Double lon, Double lat, Double alt)
        {
            longitude = lon;
            latitude = lat;
            altitude = alt;
            setReasonable(true);
        }

        /**
         * @return longitude in decimal degrees
         */
        public Double getLongitude()
        {
            return longitude;
        }

        /**
         * @param longitude in decimal degrees
         */
        public void setLongitude(Double longitude)
        {
            this.longitude = longitude;
        }

        /**
         * @return latitude in decimal degrees
         */
        public Double getLatitude()
        {
            return latitude;
        }

        /**
         * @param latitude in decimal degrees
         */
        public void setLatitude(Double latitude)
        {
            this.latitude = latitude;
        }

        /**
         * @return altitude in meters
         */
        public Double getAltitude()
        {
            return altitude;
        }

        /**
         * @param altitude in meters
         */
        public void setAltitude(Double altitude)
        {
            this.altitude = altitude;
        }
        //TODO: !!!
        //public string toString()
        //{
        //    return ReflectionToStringBuilder.toString(this);
        //}


        //public bool equals(Object o)
        //{
        //    if (this == o) return true;
        //    if (o == null) return false;
        //    if (o is Position)
        //    {
        //        Position p = (Position)o;
        //        return new EqualsBuilder().append(getLatitude(),
        //                p.getLatitude())
        //                .append(getLongitude(), p.getLongitude())
        //                .append(getAltitude(), p.getAltitude()).isEquals();
        //    }
        //    return false;
        //}

        //public int hashCode()
        //{
        //    //return new HashCodeBuilder().append(longitude)
        //    //        .append(latitude).append(altitude).toHashCode();
        //}

        /**
         * Calculates the two-dimensional great circle distance (haversine)
         * @param other position to which we calculate the distance
         * @return distance between the this and other position in meters
         */
        public Double distanceTo(Position other)
        {
            double lon0r = (Math.PI / 180) * (this.longitude);
            double lat0r = (Math.PI / 180) * (this.latitude);
            double lon1r = (Math.PI / 180) * (other.longitude);
            double lat1r = (Math.PI / 180) * (other.latitude);
            double a = Math.Pow(Math.Sin((lat1r - lat0r) / 2.0), 2);
            double b = Math.Cos(lat0r) * Math.Cos(lat1r) * Math.Pow(Math.Sin((lon1r - lon0r) / 2.0), 2);

            return 6371000.0 * 2 * Math.Asin(Math.Sqrt(a + b));
        }

        /**
         * This is used to mark positions as unreasonable if a
         * plausibility check fails during decoding. Some transponders
         * broadcast false positions and if detected, this flag is unset.
         * Note that we assume positions to be reasonable by default.
         * @return true if position has been flagged reasonable by the decoder
         */
        public bool isReasonable()
        {
            return reasonable;
        }

        /**
         * Set/unset reasonable flag.
         * @param reasonable false if position is considered unreasonable
         */
        public void setReasonable(bool reasonable)
        {
            this.reasonable = reasonable;
        }

    }
}
