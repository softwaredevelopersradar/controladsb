﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    class ParityChecker
    {
        public ParityChecker(byte aComplexState)
        {
            state = aComplexState;
        }
        //Возвращает состояние третьего бита
        public bool Unit3State
        {
            get
            {
                //Получить промежуточный результат, наложив маску битовую маску 00000100 на общий статус
                int result = state & 0x04; //0x04(в hex) это 00000100 (в bin)

                //Если результат равен 0
                if (result == 0)
                    return false; 
                else
                    return true; 
            }
        }

        private byte state;
    }
}
