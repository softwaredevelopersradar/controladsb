﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADSBControl
{
    /// <summary>
    /// A converter that takes in a boolean and returns a <see cref="Visibility"/>
    /// </summary>
    public class ByteArrayToStringConverter : BaseValueConverter<ByteArrayToStringConverter>
    {
        private static char[] hexDigits =
        {'0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            char[] whiteSpaces = {' ',' ',' ',' '};

            if (value != null)
            {
                byte[] bytes = (byte[])value;
                int l = bytes.Length;
                char[] finalArray = new char[6 + whiteSpaces.Length];
                char[] out1 = new char[l << 1];

                for (int i = 0, j = 0; i < l; i++)
                {
                    out1[j++] = hexDigits[(0xF0 & bytes[i]) >> 4];
                    out1[j++] = hexDigits[0x0F & bytes[i]];
                }
                Array.Copy(whiteSpaces, finalArray, whiteSpaces.Length);
                Array.Copy(out1, 0, finalArray, whiteSpaces.Length, out1.Length);
                return new string(finalArray);
            }
            else
                return string.Empty;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
