﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempADSBLibrary
{
    public static class TempEvents
    {
        public delegate void PlaneDataEventHandler(string iCAO, double latitude, double longitude, double altitude);
        public static event PlaneDataEventHandler OnReceivePlaneData;

        public delegate void PlaneShortDataEventHandler(string iCAO, double altitude);
        public static event PlaneShortDataEventHandler OnReceiveShortPlaneData;

        public delegate void ADSBConnectEventHandler(bool connected);
        public static event ADSBConnectEventHandler OnIsConnected;

        public static void ReceivePlaneData(string iCAO, double latitude, double longitude, double altitude)
        {
            if (OnReceivePlaneData != null)
            {
                OnReceivePlaneData (iCAO, latitude, longitude, altitude);
            }
                
        }

        public static void ReceiveShortPlaneData(string iCAO, double altitude)
        {
            if (OnReceiveShortPlaneData != null)
            {
                OnReceiveShortPlaneData(iCAO,  altitude);
            }

        }

        public static void IsConnected(bool connected)
        {
            if (OnIsConnected != null)
                OnIsConnected(connected);
        }
    }
}
