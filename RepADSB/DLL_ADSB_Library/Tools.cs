﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL_ADSB_Library
{
    public class Tools
    {
        private static char[] hexDigits =
        {'0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        /**
         * Converts a byte into a hex string (e.g. 164 -&gt; "a4")
         * @param b input byte
         * @return hex representation of input byte
         */
        public static string toHexString(sbyte b)
        {
            char[] out1 = new char[2];
            out1[0] = hexDigits[(0xF0 & b) >> 4];
            out1[1] = hexDigits[0x0F & b];
            return new string(out1);
        }


        /**
         * Converts an array of bytes in a hex string; Taken from 
         * org.apache.commons.codec.binary.Hex.
         * @param bytes array of bytes
         * @return concatenated hex representation of input byte array
         */
        public static string toHexString(sbyte[] bytes)
        {
            int l = bytes.Length;
            char[] out1 = new char[l << 1];

            for (int i = 0, j = 0; i < l; i++)
            {
                out1[j++] = hexDigits[(0xF0 & bytes[i]) >> 4];
                out1[j++] = hexDigits[0x0F & bytes[i]];
            }
            return new string(out1);
        }

        /**
         * Compares two byte arrays element by element
         * @param array1 first array
         * @param array2 second array
         * @return array1 == array2
         */
        public static bool areEqual(sbyte[] array1, sbyte[] array2)
        {
            if (array1.Length != array2.Length) return false;

            for (int i = 0; i < array1.Length; ++i)
                if (array1[i] != array2[i]) return false;

            return true;
        }

        /**
         * Compares two byte arrays element by element
         * @param array1 first array
         * @param array2 second array
         * @return array1 == array2
         */
        public static bool areEqual(char[] array1, char[] array2)
        {
            if (array1.Length != array2.Length) return false;

            for (int i = 0; i < array1.Length; ++i)
                if (array1[i] != array2[i]) return false;

            return true;
        }

        /**
         * @param byte1 first byte
         * @param byte2 second byte
         * @return byte1 xor byte2 (bitwise)
         */
        public static sbyte xor(sbyte byte1, sbyte byte2)
        {
            return (sbyte)(0xff & (byte1 ^ byte2));
        }

        /**
         * @param array1 first array
         * @param array2 second array
         * @return array1 xor array2 (bitwise)
         */
        public static sbyte[] xor(sbyte[] array1, sbyte[] array2)
        {
            Trace.Assert(array1.Length == array2.Length);

            sbyte[] res = new sbyte[array1.Length];
            for (int i = 0; i < array1.Length; ++i)
                res[i] = xor(array1[i], array2[i]);

            return res;
        }

        /**
         * Checks whether a byte array just contains elements equal to zero
         * @param  bytes input byte array
         * @return true if all bytes of the array are 0
         */
        public static bool isZero(byte[] bytes)
        {
            int x = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                x |= bytes[i];
            }
            return x == 0;
        }
    }
}
