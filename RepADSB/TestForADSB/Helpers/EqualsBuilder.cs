﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace TestForADSB
//{
//    public class EqualsBuilder
//    {
//        private bool isEqualsProp = true;

//        public EqualsBuilder()
//        {
//        }

//        public static bool reflectionEquals(Object lhs, Object rhs)
//        {
//            return reflectionEquals(lhs, rhs, false, (object)null, (String[])null);
//        }

//        //public static bool reflectionEquals(Object lhs, Object rhs, Collection excludeFields)
//        //{
//        //    return reflectionEquals(lhs, rhs, ReflectionToStringBuilder.toNoNullStringArray(excludeFields));
//        //}

//        public static bool reflectionEquals(Object lhs, Object rhs, String[] excludeFields)
//        {
//            return reflectionEquals(lhs, rhs, false, (object)null, excludeFields);
//        }

//        public static bool reflectionEquals(Object lhs, Object rhs, bool testTransients)
//        {
//            return reflectionEquals(lhs, rhs, testTransients, (object)null, (String[])null);
//        }

//        public static bool reflectionEquals(Object lhs, Object rhs, bool testTransients, object reflectUpToClass)
//        {
//            return reflectionEquals(lhs, rhs, testTransients, reflectUpToClass, (String[])null);
//        }

//        public static bool reflectionEquals(Object lhs, Object rhs, bool testTransients, object reflectUpToClass, String[] excludeFields)
//        {
//            if (lhs == rhs)
//            {
//                return true;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                object lhsClass = lhs.GetType();
//                object rhsClass = rhs.GetType();
//                object testClass;
//                if (lhsClass.GetType().IsInstanceOfType(rhs))
//                {
//                    testClass = lhsClass;
//                    if (!rhsClass.GetType().IsInstanceOfType(lhs))
//                    {
//                        testClass = rhsClass;
//                    }
//                }
//                else
//                {
//                    if (!rhsClass.GetType().IsInstanceOfType(lhs))
//                    {
//                        return false;
//                    }

//                    testClass = rhsClass;
//                    if (!lhsClass.GetType().IsInstanceOfType(rhs))
//                    {
//                        testClass = lhsClass;
//                    }
//                }

//                EqualsBuilder equalsBuilder = new EqualsBuilder();

//                try
//                {
//                    reflectionAppend(lhs, rhs, testClass, equalsBuilder, testTransients, excludeFields);

//                    while (testClass.getSuperclass() != null && testClass != reflectUpToClass)
//                    {
//                        testClass = testClass.getSuperclass();
//                        reflectionAppend(lhs, rhs, testClass, equalsBuilder, testTransients, excludeFields);
//                    }
//                }
//                catch 
//                {
//                    return false;
//                }

//                return equalsBuilder.isEquals();
//            }
//            else
//            {
//                return false;
//            }
//        }

//        private static void reflectionAppend(Object lhs, Object rhs, object clazz, EqualsBuilder builder, bool useTransients, String[] excludeFields)
//        {
//            Field[] fields = clazz.getDeclaredFields();
//            List excludedFieldList = excludeFields != null ? Arrays.asList(excludeFields) : Collections.EMPTY_LIST;
//            AccessibleObject.setAccessible(fields, true);

//            for (int i = 0; i < fields.Length && builder.isEqualsProp; ++i)
//            {
//                Field f = fields[i];
//                if (!excludedFieldList.contains(f.getName()) && f.getName().indexOf(36) == -1 && (useTransients || !Modifier.isTransient(f.getModifiers())) && !Modifier.isStatic(f.getModifiers()))
//                {
//                    try
//                    {
//                        builder.append(f.get(lhs), f.get(rhs));
//                    }
//                    catch 
//                    {
//                        //throw new InternalError("Unexpected IllegalAccessException");
//                        Console.WriteLine("Unexpected IllegalAccessException");
//                    }
//                }
//            }

//        }

//        public EqualsBuilder appendSuper(bool superEquals)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = superEquals;
//                return this;
//            }
//        }

//        public EqualsBuilder append(Object lhs, Object rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                object lhsClass = lhs.GetType();
//                if (!lhsClass.isArray())
//                {
//                    this.isEqualsProp = lhs.equals(rhs);
//                }
//                else if (lhs.GetType() != rhs.GetType())
//                {
//                    this.setEquals(false);
//                }
//                else if (lhs is long[]) {
//                    this.append((long[])lhs, (long[])rhs);
//                } else if (lhs is int[]) {
//                    this.append((int[])lhs, (int[])rhs);
//                } else if (lhs is short[]) {
//                    this.append((short[])lhs, (short[])rhs);
//                } else if (lhs is char[]) {
//                    this.append((char[])lhs, (char[])rhs);
//                } else if (lhs is byte[]) {
//                    this.append((byte[])lhs, (byte[])rhs);
//                } else if (lhs is double[]) {
//                    this.append((double[])lhs, (double[])rhs);
//                } else if (lhs is float[]) {
//                    this.append((float[])lhs, (float[])rhs);
//                } else if (lhs is bool[]) {
//                    this.append((bool[])lhs, (bool[])rhs);
//                } else {
//                    this.append((Object[])lhs, (Object[])rhs);
//                }

//                return this;
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(long lhs, long rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = lhs == rhs;
//                return this;
//            }
//        }

//        public EqualsBuilder append(int lhs, int rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = lhs == rhs;
//                return this;
//            }
//        }

//        public EqualsBuilder append(short lhs, short rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = lhs == rhs;
//                return this;
//            }
//        }

//        public EqualsBuilder append(char lhs, char rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = lhs == rhs;
//                return this;
//            }
//        }

//        public EqualsBuilder append(byte lhs, byte rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = lhs == rhs;
//                return this;
//            }
//        }

//        public EqualsBuilder append(double lhs, double rhs)
//        {
//            return !this.isEqualsProp ? this : this.append(BitConverter.ToInt64(BitConverter.GetBytes(lhs), 0),
//                BitConverter.ToInt64(BitConverter.GetBytes(rhs), 0));
//        }

//        public EqualsBuilder append(float lhs, float rhs)
//        {
//            return !this.isEqualsProp ? this : this.append(BitConverter.ToInt32(BitConverter.GetBytes(lhs), 0),
//                BitConverter.ToInt32(BitConverter.GetBytes(rhs), 0));
//        }

//        public EqualsBuilder append(bool lhs, bool rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else
//            {
//                this.isEqualsProp = lhs == rhs;
//                return this;
//            }
//        }

//        public EqualsBuilder append(Object[] lhs, Object[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(long[] lhs, long[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(int[] lhs, int[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(short[] lhs, short[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(char[] lhs, char[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(byte[] lhs, byte[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(double[] lhs, double[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(float[] lhs, float[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public EqualsBuilder append(bool[] lhs, bool[] rhs)
//        {
//            if (!this.isEqualsProp)
//            {
//                return this;
//            }
//            else if (lhs == rhs)
//            {
//                return this;
//            }
//            else if (lhs != null && rhs != null)
//            {
//                if (lhs.Length != rhs.Length)
//                {
//                    this.setEquals(false);
//                    return this;
//                }
//                else
//                {
//                    for (int i = 0; i < lhs.Length && this.isEqualsProp; ++i)
//                    {
//                        this.append(lhs[i], rhs[i]);
//                    }

//                    return this;
//                }
//            }
//            else
//            {
//                this.setEquals(false);
//                return this;
//            }
//        }

//        public bool isEquals()
//        {
//            return this.isEqualsProp;
//        }

//        protected void setEquals(bool isEqualsProp)
//        {
//            this.isEqualsProp = isEqualsProp;
//        }
//    }
//}
