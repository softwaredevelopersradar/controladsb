﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADSBControl
{
    public static class SimpleConverterICAOToString
    {
        private static char[] hexDigits =
        {'0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        public static string  Convert(object value)
        {
            if (value != null)
            {
                byte[] bytes = (byte[])value;
                int l = bytes.Length;
                char[] finalArray = new char[l];
                char[] out1 = new char[l << 1];

                for (int i = 0, j = 0; i < l; i++)
                {
                    out1[j++] = hexDigits[(0xF0 & bytes[i]) >> 4];
                    out1[j++] = hexDigits[0x0F & bytes[i]];
                }
                return new string(out1);
            }
            else
                return string.Empty;
        }
    }
}
