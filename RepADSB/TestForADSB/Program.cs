﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TestForADSB
{
    public class Program
    {


        public static Dictionary<string, string> oddDictionary = new Dictionary<string, string>();
        public static Dictionary<string, string> evenDictionary = new Dictionary<string, string>();

        static void Main(string[] args)
        {
            string pureTest_Datata_ODD = ("8D42498B58B980BCC3443C3E2F3B");// real data
            string pureTest_Datata_EVEN = ("8D42498B58B98420571D559C450C");// real data

            //string test_Datata_ODD = ("8d75804b580ff2cf7e9ba6f701d0");
            //string test_Datata_EVEN = ("8d75804b580ff6b283eb7a157117");

            byte[] bytes;
            bool parityFlag;



            //ModeSReply odd = Decoder.genericDecoder("8d75804b580ff2cf7e9ba6f701d0");
            //ModeSReply even = Decoder.genericDecoder("8d75804b580ff6b283eb7a157117");


            // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART // WTF PART  // WTF PART  // WTF PART  // WTF PART



            ModeSReply odd = Decoder.genericDecoder("8d75804b580ff2cf7e9ba6f701d0");
            ModeSReply even = Decoder.genericDecoder("8d75804b580ff6b283eb7a157117");

            //// now we know it's an airborne position message
            AirbornePositionMsg odd_pos = (AirbornePositionMsg)odd;
            AirbornePositionMsg even_pos = (AirbornePositionMsg)even;
            Position lat_lon = odd_pos.getGlobalPosition(even_pos);
            double altitude = odd_pos.getAltitude();


            //ModeSReply od11d = Decoder.genericDecoder("8d75804b580ff2cf7e9ba6f701d0");
            //ModeSReply even22 = Decoder.genericDecoder("8d75804b580ff6b283eb7a157117");

            AirbornePositionMsg odd_pos444 = new AirbornePositionMsg(pureTest_Datata_ODD);
            AirbornePositionMsg even_pos444 = new AirbornePositionMsg(pureTest_Datata_EVEN);
            ExtendedSquitter tttttestBOOSS = (ExtendedSquitter)even_pos444;

            AirbornePositionMsg tttttest = (AirbornePositionMsg)tttttestBOOSS;
            // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART  // WTF PART // WTF PART  // WTF PART  // WTF PART  // WTF PART

            AirbornePositionMsg odd_pos2222 = (AirbornePositionMsg)odd;
            AirbornePositionMsg even_pos444V = (AirbornePositionMsg)even;
            Position lat_lon22 = odd_pos2222.getGlobalPosition(even_pos444V);
            double altitude33 = odd_pos2222.getAltitude();

            Console.WriteLine("Latitude  = " + lat_lon22.getLatitude() + "°\n" +
                    "Longitude = " + lat_lon22.getLongitude() + "°\n" +
                    "Altitude  = " + altitude33 + "m");



            // AT THAT MOMENT ITS WORK - BUT IT SHOULD WORK BETTER!!!!!!!!!!!!!

            //AirbornePositionMsg odd_pos = new AirbornePositionMsg(pureTest_Datata_ODD);
            //AirbornePositionMsg even_pos = new AirbornePositionMsg(pureTest_Datata_EVEN);
            //Position lat_lon = odd_pos.getGlobalPosition(even_pos);
            //double altitude = odd_pos.getAltitude();

            Console.WriteLine("Latitude  = " + lat_lon.getLatitude() + "°\n" +
                    "Longitude = " + lat_lon.getLongitude() + "°\n" +
                    "Altitude  = " + altitude + "m");


            try
            {
                //sMasHEX = Encoding.ASCII.GetString(mess);
                //sMasHEX = new Regex(@"[^0-9ABCDEF]").Replace(sMasHEX, ""); // удалить все символы кроме цифр и букв
                List<string> testData = new List<string>();
                TcpClient client = new TcpClient("192.168.1.11", 30005);
                NetworkStream ns = client.GetStream();
                while (true)
                {
                    if (client.ReceiveBufferSize > 0)
                    {
                        bytes = new byte[client.ReceiveBufferSize];
                        ns.Read(bytes, 0, client.ReceiveBufferSize);
                        string msg = Encoding.ASCII.GetString(bytes); //the message incoming
                        msg = msg.Replace("\0", "");
                        msg = msg.Replace("\n\r*", "");
                        string[] parts1 = msg.Replace(";", "+").Split('+');
                        foreach (string singleString in parts1)
                        {
                            Console.WriteLine(singleString);
                            if (singleString.StartsWith("8D") && singleString.Length > 26)
                            {
                                parityFlag = ExtractParityBit(singleString);
                                ParitySorter(parityFlag, singleString);

                            }
                        }


                    }
                    //if (testData.Count > 60)
                    //    break;
                }
                //foreach (string singleData in testData)
                //{
                //    Console.WriteLine(singleData);
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.ReadLine();
        }

        private static void ParitySorter(bool parityFlag, string singleString)
        {
            if (parityFlag == true)
            {
                if (!oddDictionary.ContainsKey(ExtractDataForPlaneDefinition(singleString)))
                {
                    oddDictionary.Add(ExtractDataForPlaneDefinition(singleString), singleString);
                }
                CompareDictionarys(ExtractDataForPlaneDefinition(singleString), parityFlag);
            }
            else
            {
                if (!evenDictionary.ContainsKey(ExtractDataForPlaneDefinition(singleString)))
                {
                    evenDictionary.Add(ExtractDataForPlaneDefinition(singleString), singleString);
                }

                CompareDictionarys(ExtractDataForPlaneDefinition(singleString), parityFlag);
            }
        }

        private static void CompareDictionarys(string planeDefinition, bool parityFlag)
        {
            string curr_plane_ODD;
            string curr_plane_EVEN;
            if (parityFlag == true)
            {
                if (evenDictionary.ContainsKey(planeDefinition))
                {
                    evenDictionary.TryGetValue(planeDefinition, out curr_plane_EVEN);
                    oddDictionary.TryGetValue(planeDefinition, out curr_plane_ODD);
                    Decode(curr_plane_EVEN, curr_plane_ODD);
                }
            }
            else
            {
                if (oddDictionary.ContainsKey(planeDefinition))
                {
                    evenDictionary.TryGetValue(planeDefinition, out curr_plane_EVEN);
                    oddDictionary.TryGetValue(planeDefinition, out curr_plane_ODD);
                    Decode(curr_plane_EVEN, curr_plane_ODD);
                }
            }
        }

        private static void Decode(string curr_plane_EVEN, string curr_plane_ODD)
        {
            ModeSReply msg555 = new ModeSReply(curr_plane_EVEN);
            AirbornePositionMsg odd_pos = new AirbornePositionMsg(curr_plane_ODD);
            AirbornePositionMsg even_pos = new AirbornePositionMsg(curr_plane_EVEN);
            Position lat_lon = odd_pos.getGlobalPosition(even_pos);
            double altitude = odd_pos.getAltitude();

            Events.ReceivePlaneData(Tools.toHexString(msg555.getIcao24()), lat_lon.getLatitude(), lat_lon.getLongitude(), altitude);

            Console.WriteLine("ICAO  = " + Tools.toHexString(msg555.getIcao24()) + "\n" +
                "Latitude  = " + lat_lon.getLatitude() + "°\n" +
                    "Longitude = " + lat_lon.getLongitude() + "°\n" +
                    "Altitude  = " + altitude + "m" + "\n");
        }

        private static bool ExtractParityBit(string singleString)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buffer;
            sb.Append(singleString);
            sb.Remove(0, 13);
            sb.Remove(1, singleString.Length - 14);
            buffer = Encoding.ASCII.GetBytes(sb.ToString());
            ParityChecker parityFlag = new ParityChecker(buffer[0]);
            return parityFlag.Unit3State;
        }

        private static string ExtractDataForPlaneDefinition(string singleString)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(singleString);
            sb.Remove(13, singleString.Length - 13);
            return sb.ToString();
        }
    }





}
