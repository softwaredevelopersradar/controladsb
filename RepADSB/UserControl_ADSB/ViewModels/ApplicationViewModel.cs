﻿using ModelsTablesDBLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace ADSBControl
{
    public class ApplicationViewModel : BaseViewModel
    {

        public ObservableCollection<Plane> Planes { get; set; } //= new ObservableCollection<Plane>();
        private Dictionary<string, List<string>> deserializedPlanesByJson = new Dictionary<string, List<string>>();
        private Dictionary<string, List<string>> curPlanesExpandedData = new Dictionary<string, List<string>>();


        //private string basePath = AppDomain.CurrentDomain.BaseDirectory + "testByDBFULL.json";
        private string basePath = AppDomain.CurrentDomain.BaseDirectory + "planesData.json";
        private double curSizeOfWindow;


        // команда добавления нового объекта
        public ICommand PrintCommand { get; set; }
        public ICommand SaveToWordCommand { get; set; }

        public ICommand DeleteAllRecords { get; set; }

        #region counters


        List<string> ICAOWithFullData = new List<string>();
        List<string> ICAOWithOutFullData = new List<string>();

        private int counterWithFullData;
        private int counterWithOutFullData;
        double startSizeOfControl;

        //№ planes w/o coordinates
        private int counter1;
        //№ planes with coordinates
        private int counter2;

        private int rowCounter;
        private int rowDataCounter;
        private int prevRowCounter;
        private int emptyRowCounter;
        private int curRowCounter;

        public int Counter1
        {
            get { return counter1; }
            set
            {
                counter1 = value;
                OnPropertyChanged("Counter1");
            }
        }

        public int Counter2
        {
            get { return counter2; }
            set
            {
                counter2 = value;
                OnPropertyChanged("Counter2");
            }
        }

        #endregion

        public ApplicationViewModel()
        {
            Planes = new ObservableCollection<Plane>();
            PrintCommand = new RelayCommand(PrintMethod);
            SaveToWordCommand = new RelayCommand(SaveToWordMethod);
            DeleteAllRecords = new RelayCommand(DeleteAllRecordsMethod);

            //cleaner = new ADSBCleanerEvent();
            startSizeOfControl = System.Windows.SystemParameters.PrimaryScreenHeight;

            LoadDataFromJson();
        }

        public void DrawEmptyRows(double startSize)
        {
            try
            {
                curSizeOfWindow = startSize;
                rowCounter = Convert.ToInt32((startSize - 90) / 29);

                curRowCounter = rowCounter - prevRowCounter;
                //Application.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                //{

                if (Planes == null)
                    this.Planes = new ObservableCollection<Plane>();
                if (curRowCounter > 0 && rowDataCounter < rowCounter)
                {
                    for (int i = 0; i < curRowCounter; i++)
                    {
                        Planes.Add(new Plane());
                        emptyRowCounter++;
                    }
                }
                if (curRowCounter < 0 && emptyRowCounter > 0)
                {
                    for (int i = curRowCounter; i < 0; i++)
                    {
                        if (emptyRowCounter > 0)
                        {
                            Planes.RemoveAt(Planes.Count - 1);
                            emptyRowCounter--;
                        }
                    }

                }

                prevRowCounter = rowCounter;
            }
            catch (Exception epx)
            {

            }

            //});

        }



        private void SaveToWordMethod()
        {
            MessageBox.Show("function is not ready");
        }

        private void PrintMethod()
        {
            MessageBox.Show("this function is not ready");
        }

        // Lena
        private void DeleteAllRecordsMethod()
        {
            ADSBCleanerEvent.CleanADSBTable(this, EventArgs.Empty);
        }

        //pol for Lena
        internal void ClearAll()
        {
            Planes.Clear();
            Counter1 = 0;
            Counter2 = 0;
            counterWithFullData = 0;
            counterWithOutFullData = 0;
            rowCounter = 0;
            rowDataCounter = 0;
            prevRowCounter = 0;
            emptyRowCounter = 0;
            curRowCounter = 0;
            curPlanesExpandedData.Clear();
            ICAOWithOutFullData.Clear();
            ICAOWithFullData.Clear();
            DrawEmptyRows(curSizeOfWindow);
        }


        public void DeleteRecord(string curICAO)
        {
            try
            {
                Plane curPlane = Planes.Where(y => y.ICAO != null).FirstOrDefault(plane => plane.ICAO.Equals(curICAO));
                //Planes.RemoveAt(0);
                if (curPlane != null)
                {
                    // Lena
                    if (curPlane.Longitude == -1)
                    //if(curPlane.DataField=="        -                   -          ")
                    {
                            Counter1--;
                        counterWithOutFullData--;
                        rowDataCounter--;
                        if (ICAOWithOutFullData.Contains(curICAO))
                            ICAOWithOutFullData.Remove(curICAO);
                    }
                    else
                    {
                        Counter2--;
                        counterWithFullData--;
                        rowDataCounter--;
                        if (ICAOWithFullData.Contains(curICAO))
                            ICAOWithFullData.Remove(curICAO);
                        if (ICAOWithOutFullData.Contains(curICAO))
                            ICAOWithOutFullData.Remove(curICAO);
                    }
                    Planes.Remove(curPlane);

                    if (curRowCounter > 0 || rowCounter > emptyRowCounter + rowDataCounter)
                    //Draw empty raw
                    {
                        Planes.Add(new Plane());
                        emptyRowCounter++;
                    }
                }
            }
            catch { }
        }

        private void AddPlaneWithFullDataToTable(string curICAO, double curLatitude, double curLongitude,
            double curAltitude, List<string> curExpandedPlaneData)
        {

            string testURI = null;
            string country = string.Empty;
            string typeOfPlane = string.Empty;

            if (curExpandedPlaneData != null)
            {
                if (curExpandedPlaneData[5] != null)
                {
                    country = curExpandedPlaneData[5];
                    testURI = "pack://application:,,,/ADSBControl;component/CountryFlags/" + country + ".png";
                }
                if (curExpandedPlaneData[1] != null)
                    //typeOfPlane = SetTypeOfPlane(int.Parse(curExpandedPlaneData[1]));
                    typeOfPlane = curExpandedPlaneData[1];
            }

            if (Planes == null)
            {
                Planes = new ObservableCollection<Plane>();
            }

            string curStringICAO = curICAO;

            if (!ICAOWithOutFullData.Contains(curStringICAO) && !ICAOWithFullData.Contains(curStringICAO))
            {
                ICAOWithOutFullData.Add(curStringICAO);
                ICAOWithFullData.Add(curStringICAO);
                Counter2 = ++counterWithFullData;
            }
            if (ICAOWithOutFullData.Contains(curStringICAO) && !ICAOWithFullData.Contains(curStringICAO))
            {
                ICAOWithFullData.Add(curStringICAO);
                Counter2 = ++counterWithFullData;
                Counter1 = --counterWithOutFullData;
            }

            Plane curPlane = Planes.Where(y => y.ICAO != null).FirstOrDefault(plane => plane.ICAO.Equals(curICAO));

            if (curPlane != null)
            {
                curPlane.DataField = curLatitude.ToString("N6") + "    " + curLongitude.ToString("N6");
                curPlane.Altitude = "    " + Math.Round(curAltitude, 0).ToString();

                // Lena
                curPlane.Latitude = curLatitude;
                curPlane.Longitude = curLongitude;
            }
            else
            {
                Application.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    Planes.Insert(0,
                    new Plane
                    {
                        ICAO = curICAO,
                        CountryFlag = testURI,
                        TypeOfPlane = typeOfPlane,
                        DataField = curLatitude.ToString("N6") + "    " + curLongitude.ToString("N6"),

                        // Lena
                        Latitude = curLatitude,
                        Longitude = curLongitude,

                        Altitude = "    " + Math.Round(curAltitude, 0).ToString(),
                        Country = country
                    });

                    rowDataCounter = Counter1 + Counter2;
                    if (emptyRowCounter > 0)
                    {
                        Planes.RemoveAt(Planes.Count - 1);
                        emptyRowCounter--;
                    }


                });
            }



        }

        private void AddPlaneWithShortDataToTable(string curICAO, double curAltitude, List<string> curExpandedPlaneData)
        {

            string testURI = null;
            string country = string.Empty;
            string typeOfPlane = string.Empty;

            if (curExpandedPlaneData != null)
            {
                if (curExpandedPlaneData[5] != null)
                {
                    country = curExpandedPlaneData[5];
                    testURI = "pack://application:,,,/ADSBControl;component/CountryFlags/" + country + ".png";
                }
                if (curExpandedPlaneData[1] != null)
                    //typeOfPlane = SetTypeOfPlane(int.Parse(curExpandedPlaneData[1]));
                    typeOfPlane = curExpandedPlaneData[1];

            }
            if (Planes == null)
                Planes = new ObservableCollection<Plane>();

            string curStringICAO = curICAO;

            if (!ICAOWithOutFullData.Contains(curStringICAO) && !ICAOWithFullData.Contains(curStringICAO))
            {
                ICAOWithOutFullData.Add(curStringICAO);
                Counter1 = ++counterWithOutFullData;
            }

            if (Planes.Where(y => y.ICAO != null).Any(plane => plane.ICAO.Equals(curICAO)))
            {
                //////refresh
                Plane curPlane = Planes.Where(y => y.ICAO != null).FirstOrDefault(plane => plane.ICAO.Equals(curICAO));

                if (curPlane != null)
                {
                    //curPlane.Altitude = Math.Round(curAltitude, 0);
                    curPlane.Altitude = "    " + Math.Round(curAltitude, 0).ToString();
                    //Events.OnReceivePlaneData -= AddPlaneToTable;

                    // Lena
                   // curPlane.Latitude = -1;
                   // curPlane.longitude = -1;

                }
            }
            else
            {
                Application.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    Planes.Insert(0,
               new Plane
               {
                   ICAO = curICAO,
                   CountryFlag = testURI,
                   TypeOfPlane = typeOfPlane,
                   DataField = "        -                   -          ",
                   Altitude = "    " + Math.Round(curAltitude, 0).ToString(),

                   // Lena
                   Latitude = -1,
                   Longitude = -1,

                    Country = country
               });
                    rowDataCounter = Counter1 + Counter2;
                    if (emptyRowCounter > 0)
                    {
                        Planes.RemoveAt(Planes.Count - 1);
                        emptyRowCounter--;
                    }
                });
                //Events.OnReceiveShortPlaneData -= AddPlaneWithShortDataToTable;
            }
        }

        public void AddPlaneToTable(string curICAO, double curLatitude, double curLongitude, double curAltitude)
        {
            List<string> curExpandedPlaneData = GetExpandedPlaneData(curICAO);

            if (curLatitude == -1 || curLongitude == -1)
                AddPlaneWithShortDataToTable(curICAO, curAltitude, curExpandedPlaneData);
            else
                AddPlaneWithFullDataToTable(curICAO, curLatitude, curLongitude, curAltitude, curExpandedPlaneData);

        }


        public void AddRange(List<TempADSB> tempPlaneList)
        {
            tempPlaneList.ToList().ForEach(plane =>
            {
                AddPlaneToTable(plane.ICAO, plane.Coordinates.Latitude, plane.Coordinates.Longitude, plane.Coordinates.Altitude);
            });
        }

        private void LoadDataFromJson()
        {
            using (StreamReader file = File.OpenText(basePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                deserializedPlanesByJson = (Dictionary<string, List<string>>)serializer.Deserialize(file, typeof(Dictionary<string, List<string>>));
            }
        }

        private List<string> GetExpandedPlaneData(string curICAO)
        {
            if (!curPlanesExpandedData.ContainsKey(curICAO))
            {
                if (deserializedPlanesByJson.TryGetValue(curICAO, out List<string> expandedPlaneData))
                {
                    curPlanesExpandedData.Add(curICAO, expandedPlaneData);
                    return expandedPlaneData;
                }
                else return null;
            }
            else return null; //return curPlanesExpandedPlaneData[curICAO];
        }


        private string SetTypeOfPlane(int type)
        {
            string typeOfPlane = string.Empty;

            switch (type)
            {
                case (0):
                    typeOfPlane = "Самолёт";
                    break;
                case (1):
                    typeOfPlane = "Вертолёт";
                    break;
                case (2):
                    typeOfPlane = "Квадрокоптер";
                    break;
                case (3):
                    //Airport Ground Support Equipment 
                    typeOfPlane = "GSE";
                    break;
                default:
                    break;
            }
            return typeOfPlane;
        }


        // Lena *****************************************************************************
        //***

        public void DelAir(List<TempADSB> lstTempADSB)
        {
            int ii = 0;
            int index = -1;

            if (Planes.Count > 0)
            {
                for (ii = (Planes.Count - 1); ii >= 0; ii--)
                {
                    if (Planes[ii].ICAO != null)
                    {
                        index = -1;
                        index = lstTempADSB.FindIndex(x => (x.ICAO == Planes[ii].ICAO));
                        // В новом списке не нашли такой ICAO->в старом удаляем его
                        if (index < 0)
                        {
                            try
                            {
                                DeleteRecord(Planes[ii].ICAO);
                            }
                            catch { }
                        }
                    } // Planes[ii].ICAO != null
                } // for
            }
        }
        // ***************************************************************************** Lena



    }
}