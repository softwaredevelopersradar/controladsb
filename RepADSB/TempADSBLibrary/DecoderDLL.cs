﻿using System.Runtime.InteropServices;
using System.Text;

namespace TempADSBLibrary
{
    class DecoderDLL
    {
        [DllImport("DecoderDLL", CallingConvention = CallingConvention.Cdecl, EntryPoint = "createQDecoderDialog")]
        public static extern int CreateDecoder(int timePeriod, int writeLogLevel);

        [DllImport("DecoderDLL", CallingConvention = CallingConvention.Cdecl, EntryPoint = "destroyQDecoderDialog")]
        public static extern void DestroyDecoder();

        [DllImport("DecoderDLL", CallingConvention = CallingConvention.Cdecl, EntryPoint = "decode")]
        public static extern void Decode(string src, StringBuilder dst);
    }
}
