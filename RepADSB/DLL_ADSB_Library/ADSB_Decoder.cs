﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace DLL_ADSB_Library
{
    public class ADSB_Decoder
    {
        public static Dictionary<string, string> oddDictionary = new Dictionary<string, string>();
        public static Dictionary<string, string> evenDictionary = new Dictionary<string, string>();

        private TcpClient client;
        private NetworkStream ns;


        //private void TestConnection()
        //{
        //    byte[] bytes;
        //    bool parityFlag;
        //    List<string> testlist = new List<string>();

        //    try
        //    {
        //        List<string> testData = new List<string>();
        //        TcpClient client = new TcpClient("192.168.0.11", 30005);
        //        NetworkStream ns = client.GetStream();
        //        while (true)
        //        {
        //            //DoEvents();
        //            if (client.ReceiveBufferSize > 0)
        //            {
        //                bytes = new byte[client.ReceiveBufferSize];
        //                ns.Read(bytes, 0, client.ReceiveBufferSize);
        //                string msg = Encoding.ASCII.GetString(bytes); //the message incoming
        //                msg = msg.Replace("\0", "");
        //                msg = msg.Replace("\n\r*", "");
        //                string[] parts1 = msg.Replace(";", "+").Split('+');
        //                foreach (string singleString in parts1)
        //                {
        //                    //Console.WriteLine(singleString);
        //                    if (singleString.StartsWith("8D") && singleString.Length > 26)
        //                    {
        //                        testlist.Add(singleString.Substring(25));

        //                        parityFlag = ExtractParityBit(singleString);
        //                        ParitySorter(parityFlag, singleString);

        //                    }
        //                }


        //            }
        //            //if (testData.Count > 60)
        //            //    break;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Disconnect();
        //        Console.WriteLine(e.ToString());
        //    }

        //    Console.ReadLine();
        //}

        public void ConnectToADSB()
        {

            //string pureTest_Datata_ODD = ("8D42498B58B980BCC3443C3E2F3B");// real data
            //string pureTest_Datata_EVEN = ("8D42498B58B98420571D559C450C");// real data
            //AirbornePositionMsg odd_pos = new AirbornePositionMsg(pureTest_Datata_ODD);
            //AirbornePositionMsg even_pos = new AirbornePositionMsg(pureTest_Datata_EVEN);
            //Position lat_lon = odd_pos.getGlobalPosition(even_pos);
            //double altitude = odd_pos.getAltitude();

            //ModeSReply msg5545 = new ModeSReply(pureTest_Datata_EVEN);

            //ModeSReply msg2 = new ModeSReply("8D49D40358C3807A0770A07C826B");
            //AirbornePositionMsg odd_pos2 = new AirbornePositionMsg("8D49D40358C3807A0770A07C826B");
            //double altitude2 = odd_pos2.getAltitude();
            //Events.ReceiveShortPlaneData(Tools.toHexString(msg2.getIcao24()), altitude2);
            //Events.ReceivePlaneData(Tools.toHexString(msg5545.getIcao24()), lat_lon.getLatitude(), lat_lon.getLongitude(), altitude);


            byte[] bytes;
            bool parityFlag;
            List<string> testlist = new List<string>();


            try
            {
                List<string> testData = new List<string>();
                //client = new TcpClient("192.168.0.11", 30005);
                var client = new TcpClient();
                if (!client.ConnectAsync("192.168.0.11", 30005).Wait(5000))
                {
                    // connection failure
                }
                ns = client.GetStream();
                while (true)
                {
                    //DoEvents();
                    if (client.ReceiveBufferSize > 0)
                    {
                        bytes = new byte[client.ReceiveBufferSize];
                        ns.Read(bytes, 0, client.ReceiveBufferSize);
                        string msg = Encoding.ASCII.GetString(bytes); //the message incoming
                        msg = msg.Replace("\0", "");
                        msg = msg.Replace("\n\r*", "");
                        string[] parts1 = msg.Replace(";", "+").Split('+');
                        foreach (string singleString in parts1)
                        {
                            //Console.WriteLine(singleString);
                            if (singleString.StartsWith("8D") && singleString.Length > 26)
                            {
                                testlist.Add(singleString.Substring(25));

                                parityFlag = ExtractParityBit(singleString);
                                ParitySorter(parityFlag, singleString);

                            }
                        }


                    }
                    //if (testData.Count > 60)
                    //    break;
                }

            }
            catch (Exception e)
            {
                Disconnect();
                Console.WriteLine(e.ToString());
            }

            Console.ReadLine();
        }

        private static void ParitySorter(bool parityFlag, string singleString)
        {
            try
            {
                ModeSReply msg = new ModeSReply(singleString);
                AirbornePositionMsg odd_pos = new AirbornePositionMsg(singleString);
                double altitude = odd_pos.getAltitude();

                if (altitude > 6000 && altitude < 14000)
                    Events.ReceiveShortPlaneData(Tools.toHexString(msg.getIcao24()), altitude);

                if (parityFlag == true)
                {
                    if (!oddDictionary.ContainsKey(ExtractDataForPlaneDefinition(singleString)))
                    {
                        oddDictionary.Add(ExtractDataForPlaneDefinition(singleString), singleString);
                    }
                    CompareDictionarys(ExtractDataForPlaneDefinition(singleString), parityFlag);
                }
                else
                {
                    if (!evenDictionary.ContainsKey(ExtractDataForPlaneDefinition(singleString)))
                    {
                        evenDictionary.Add(ExtractDataForPlaneDefinition(singleString), singleString);
                    }

                    CompareDictionarys(ExtractDataForPlaneDefinition(singleString), parityFlag);
                }
            }
            catch (Exception e) { }

        }

        //parityFlag - show even or ODD massage
        private static void CompareDictionarys(string planeDefinition, bool parityFlag)
        {
            string curr_plane_ODD;
            string curr_plane_EVEN;
            if (parityFlag == true)
            {
                if (evenDictionary.ContainsKey(planeDefinition))
                {
                    evenDictionary.TryGetValue(planeDefinition, out curr_plane_EVEN);
                    oddDictionary.TryGetValue(planeDefinition, out curr_plane_ODD);
                    Decode(curr_plane_EVEN, curr_plane_ODD);
                }
            }
            else
            {
                if (oddDictionary.ContainsKey(planeDefinition))
                {
                    evenDictionary.TryGetValue(planeDefinition, out curr_plane_EVEN);
                    oddDictionary.TryGetValue(planeDefinition, out curr_plane_ODD);
                    Decode(curr_plane_EVEN, curr_plane_ODD);
                }
            }
        }

        private static void Decode(string curr_plane_EVEN, string curr_plane_ODD)
        {
            ModeSReply msg555 = new ModeSReply(curr_plane_EVEN);
            AirbornePositionMsg odd_pos = new AirbornePositionMsg(curr_plane_ODD);
            AirbornePositionMsg even_pos = new AirbornePositionMsg(curr_plane_EVEN);
            Position lat_lon = odd_pos.getGlobalPosition(even_pos);
            double altitude = odd_pos.getAltitude();

            if ( lat_lon.getLatitude() > 48 && lat_lon.getLatitude() < 57)
                if ( lat_lon.getLongitude() > 18 && lat_lon.getLongitude() < 35)
                    if ( altitude > 6000 && altitude < 14000)
                        Events.ReceivePlaneData(Tools.toHexString(msg555.getIcao24()), lat_lon.getLatitude(), lat_lon.getLongitude(), altitude);

        }

        private static bool ExtractParityBit(string singleString)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buffer;
            sb.Append(singleString);
            sb.Remove(0, 13);
            sb.Remove(1, singleString.Length - 14);
            buffer = Encoding.ASCII.GetBytes(sb.ToString());
            ParityChecker parityFlag = new ParityChecker(buffer[0]);
            return parityFlag.Unit3State;
        }

        private static string ExtractDataForPlaneDefinition(string singleString)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(singleString);
            sb.Remove(13, singleString.Length - 13);
            return sb.ToString();
        }


        [SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

        public static Task Delay(double milliseconds)
        {
            var tcs = new TaskCompletionSource<bool>();
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (obj, args) =>
            {
                tcs.TrySetResult(true);
            };
            timer.Interval = milliseconds;
            timer.AutoReset = false;
            timer.Start();
            return tcs.Task;
        }


        public void Disconnect()
        {
            // if there is client
            if (ns != null)
            {
                // close client stream
                try
                {
                    ns.Close();
                    ns = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (client != null)
            {
                // close client connection
                try
                {
                    client.Close();
                    client = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            //// if there is client
            //if (thrRead != null)
            //{
            //    // destroy thread for reading
            //    try
            //    {
            //        thrRead.Abort();
            //        thrRead.Join(500);
            //        thrRead = null;
            //    }
            //    catch (System.Exception)
            //    {
            //        //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            //    }

            //}

            //// generate event
            //DisconnectNet();
        }

    }
}
