﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADSBControl
{
    public class ImageToolTipConverter : BaseValueConverter<ImageConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string countryFlag = value.ToString();
                return countryFlag = countryFlag.Substring(58, countryFlag.Length - 4);
            }
            //return new BitmapImage((new Uri(value.ToString())));
            //return new BitmapImage(new Uri(value.ToString()));
            else
                return string.Empty;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
