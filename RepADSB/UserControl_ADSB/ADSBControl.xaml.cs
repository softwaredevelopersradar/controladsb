﻿using DLL_ADSB_Library;
using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ADSBControl
{

    /// <summary>
    /// Interaction logic for UserControlADSB.xaml
    /// </summary>
    public partial class ADSBControl : UserControl
    {
        //private ADSB_Decoder ADSBDecoder;
        private ApplicationViewModel curVM;

        public event EventHandler<string> OnSelectedRowADSB = (object sender, string data) => { };

        public ADSBControl()
        {

            InitializeComponent();
            this.SizeChanged += OnWindowSizeChanged;

            curVM = new ApplicationViewModel();
            DataContext = curVM;

            plane = new Plane();
            strICAO = string.Empty;

            // testName.ContentTemplateSelector = new BitmapImage(new Uri(@"D:\Print.png"));

            //testName.Source = new BitmapImage( new Uri("pack://application:,,,/WClassLibrary1;component/CountryFlags/France.png"));


            //ADSBDecoder = new ADSB_Decoder();
            //Task.Run(() => ADSBDecoder.ConnectToADSB());
        }

        
        protected void OnWindowSizeChanged(object sender, SizeChangedEventArgs e)
        {
            double newwindowheight = e.NewSize.Height;
            //double prevwindowheight = e.previoussize.height;
            curVM.DrawEmptyRows(newwindowheight);
            Task.Delay(100);

            try
            {
                if (plane.ICAO == null) { DataGridWhiteForeground(); }
                else { UpdateStyleDataGrid(); }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //public  void ADSBDecoder_Disconnect()
        //{
        //    ADSBDecoder.Disconnect();
        //}



       // Lena
       public void DEL_TAB()
        {
            curVM.ClearAll();
        }

        // Lena
        public void DelAirMain(List<TempADSB> planeList)
        {
            curVM.DelAir(planeList);
        }



        public void AddPlaneToTable(string curICAO, double curLatitude, double curLongitude, double curAltitude)
        {
            curVM.AddPlaneToTable(curICAO, curLatitude, curLongitude, curAltitude);
        }

        public void AddRange(List<TempADSB> planeList)
        {
            curVM.AddRange(planeList);
        }

        public void DeleteR(string curIcao)
        {
            curVM.DeleteRecord(curIcao);
        }



        public Plane plane { get; set; }
        public string strICAO { get; set; }
        private bool fICAO = false;
        private void firstDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                plane = (Plane)firstDataGrid.SelectedItem;

                if (plane.ICAO == null)
                {
                    OnSelectedRowADSB(this, string.Empty);

                    DataGridWhiteForeground();
                }
                else
                {
                    if(strICAO == plane.ICAO)
                    {
                        fICAO = true;
                        OnSelectedRowADSB(this, plane.ICAO);
                        DataGridWhiteForeground();
                    }
                    else
                    {
                        strICAO = plane.ICAO;
                        fICAO = false;

                        if (plane.ICAO.Length == 6)
                        {
                            OnSelectedRowADSB(this, plane.ICAO);

                            UpdateStyleDataGrid();
                        }
                    }
                }

                firstDataGrid.UnselectAllCells();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        SolidColorBrush solidBrushRed = new SolidColorBrush(Colors.Red);
        SolidColorBrush solidBrushWhite = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFD9F9E3"));
        private void UpdateStyleDataGrid()
        {
            try
            {
                if (plane.ICAO.Length == 6)
                {
                    DataGridWhiteForeground();

                    var dataGridRowSelect = firstDataGrid.ItemContainerGenerator.ContainerFromItem(plane) as DataGridRow;

                    if (dataGridRowSelect != null)
                    {
                        dataGridRowSelect.Foreground = solidBrushRed;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void DataGridWhiteForeground()
        {
            for (int row = 0; row < firstDataGrid.Items.Count; row++)
            {
                var dataGridRow = firstDataGrid.ItemContainerGenerator.ContainerFromItem(firstDataGrid.Items[row]) as DataGridRow;

                if (dataGridRow != null)
                {
                    dataGridRow.Foreground = solidBrushWhite;
                }
            }
        }

        private void firstDataGrid_LayoutUpdated(object sender, EventArgs e)
        {
            try
            {
                if (fICAO) return;

                if (plane.ICAO == null) { DataGridWhiteForeground(); }
                else { UpdateStyleDataGrid(); }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
